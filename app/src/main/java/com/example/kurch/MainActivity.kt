package com.example.kurch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(this, "hello world", Toast.LENGTH_SHORT).show();
        setContentView(R.layout.activity_main)
    }
}
